# Happy Job: Question Form

[Описание бизнес-логики](docs/README.md)

Для работы приложения обязательно запустить `npm run mock`.

#### NPM scrips

```
npm install   - установить зависимости
npm run serve - сборка и горячая перезагрузка
npm run build - сборка для production с минификацией
npm run mock  - json-server с тестовыми данными
npm run lint  - линтиг скриптов с помощью eslint
```

#### Роутинг

```
/           - Стартовая страница
/activities - Страница выбора направлений и функций
/evaluation - Страница оценки функций
/end        - Страница завершения опроса
```

#### API

```
GET /isfilled - Получить статус заполненности формы
{
    "isfilled": false
}

GET /activities - Получить все направления деятельности
[
    {
        "id": 1,
        "title": "Предоставление IT-сервисов",
        "icon": "http://localhost:3000/images/computer.svg"
    }
]

GET /functions - Получить все функциональные обязанности
[
    {
        "id": 1,
        "activityId": 1,
        "title": "Создание рабочих мест для новых сотрудников офисов КЦ, СЦ НН",
        "filled": false,
        "selected": false,
        "hint": "Обеспечение SLA и развития базовых сервисов ИТ: телефония, почта, wifi, проводная сеть и др.",
        "periodicity": null,
        "processTime": null,
        "helpfulness": null,
        "resultQuality": null,
        "comment": ""
    }
]

PUT /activities - Заменить все направления деятельности
PUT /functions  - Заменить все функциональные обязанности

```
