import '@babel/polyfill';
import './polyfills/requestAnimationFrame';

import Vue from 'vue';
import Vuebar from 'vuebar';
import VTooltip from './libs/vtooltip';

import store from './store';
import router from './router';

import App from './App.vue';

Vue.config.productionTip = false;

Vue.use(Vuebar);
Vue.use(VTooltip);

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app');
