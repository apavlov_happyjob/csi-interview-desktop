import Vue from 'vue';
import Router from 'vue-router';

import StartContainer from './container/StartContainer.vue';
import ActivitiesContainer from './container/ActivitiesContainer.vue';
import FunctionsContainer from './container/FunctionsContainer.vue';
import EndContainer from './container/EndContainer.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/csi/desktop',
      name: 'start',
      component: StartContainer,
    },
    {
      path: '/csi/desktop/activities',
      name: 'activities',
      component: ActivitiesContainer,
    },
    {
      path: '/csi/desktop/evaluation',
      name: 'evaluation',
      component: FunctionsContainer,
    },
    {
      path: '/csi/desktop/end',
      name: 'end',
      component: EndContainer,
    },
    {
      path: '*',
      redirect: '/csi/desktop',
    },
  ],
});
