import $ from 'jquery';
import '../polyfills/jquery.xdomainrequest.min';

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isDataLoaded: false,
    minScore: 5,
    activeFunction: null,
    errorsActiveFunction: {
      periodicity: false,
      processTime: false,
      helpfulness: false,
      resultQuality: false,
      comment: false,
    },
    offsetTopActiveFunction: 0,
    userParams: {
      cid: null,
      week: null,
      pid: null,
      hash: null,
      lang: null,
    },
    currentHints: [],
    hints: [
      {
        id: 1,
        type: 'hint',
        latency: 8000,
        message: '<p>Используйте прокрутку, чтобы увидеть весь список функциональных областей и&nbsp;направлений деятельности.</p><p>Вы&nbsp;можете выбрать несколько направлений в&nbsp;каждой функциональной области.</p>',
      },
      {
        id: 2,
        type: 'hint',
        latency: 4000,
        message: '<p>Наведите на&nbsp;знак вопроса, чтобы увидеть краткое описание направления деятельности.</p>',
      },
      {
        id: 3,
        type: 'hint',
        latency: 4000,
        message: '<p>Для удобства навигации Вы&nbsp;можете скрывать и&nbsp;раскрывать направления деятельности отдельных функциональных областей.</p>',
      },
      {
        id: 4,
        type: 'hint',
        message: '<p>Оцените направления деятельности, которые&nbsp;Вы выбрали.</p>',
      },
      {
        id: 5,
        type: 'error',
        message: '<p>Оцените выбранные направления по&nbsp;4&nbsp;критериям.</p>',
      },
      {
        id: 6,
        type: 'error',
        message: '<p>При наличии оценки от&nbsp;5&nbsp;и&nbsp;ниже баллов, необходимо оставить комментарий.</p>',
      },
      {
        id: 7,
        type: 'hint',
        message: '<p>Вы&nbsp;можете оставить комментарий к&nbsp;Вашей оценке. Благодарности и&nbsp;предложения&nbsp;&mdash; лучший способ дать понять, кого похвалить и&nbsp;что улучшить.</p>',
      },
    ],
    activities: [],
    functions: [],
  },
  getters: {
    /**
     * Возвращает количество выбранных Ф.О.
     * @param state
     * @param getters
     * @returns {number}
     */
    getCountFunctions(state, getters) {
      return getters.getSelectedFunctions.length;
    },

    /**
     * Возвращает индекс оцениваемой Ф.О. в массиве выбранных Ф.О.
     * @param state
     * @param getters
     * @returns {function(*): (index|number|never|*)}
     */
    getIndexFunctionById: (state, getters) => id => getters.getSelectedFunctions.findIndex(item => item.id === id),

    /**
     * Возвращает Ф.О. из массива с заданным индексом.
     * @param state
     * @param getters
     * @returns {function(*): (*|null)}
     */
    getFunctionByIndex: (state, getters) => index => getters.getSelectedFunctions[index] || null,

    /**
     * Возвращает массив выбранных Ф.О.
     * @param state
     * @returns {[]}
     */
    getSelectedFunctions(state) {
      return state.functions.filter(item => item.selected);
    },

    /**
     * Возвращает массив выбранных Н.Д.
     * @param state
     * @param getters
     * @returns {[]}
     */
    getSelectedActivities(state, getters) {
      return state.activities.filter(activity => getters.getSelectedFunctions.some(item => item.activityId === activity.id));
    },

    /**
     * Возвращает массив идентификаторов выбранных Ф.О.
     * @param state
     * @param getters
     * @returns {*}
     */
    getIdsSelectedFunctions(state, getters) {
      return getters.getSelectedFunctions.map(item => item.id);
    },

    /**
     * Возвращает массивы выбраных Ф.О. по ID Н.Д.
     * @param state
     * @param getters
     * @returns {function(*): *}
     */
    getSelectedFunctionsByActivityId: (state, getters) => id => getters.getSelectedFunctions.filter(item => item.activityId === id),
  },
  mutations: {
    /**
     * Устанавливает значение состояния загрузки данных.
     * @param state
     * @param boolean
     */
    setDataLoaded(state, boolean) {
      state.isDataLoaded = boolean;
    },

    /**
     * Устанавливает новый массив Н.Д.
     * @param state
     * @param activities
     */
    setActivities(state, activities) {
      state.activities = activities;
    },

    /**
     * Устанавливает новый массив Ф.О.
     * @param state
     * @param functions
     */
    setFunctions(state, functions) {
      state.functions = functions;
    },

    /**
     * Изменяет параметры Ф.О. по указанному ID.
     * @param state
     * @param payload
     */
    changeFuncState(state, payload) {
      state.activeFunction = Object.assign({}, state.activeFunction, payload);

      state.functions = state.functions.map((item) => {
        if (item.id !== payload.id) {
          return item;
        }

        return Object.assign({}, item, payload);
      });
    },

    /**
     * Устанавливают оцениваемую Ф.О.
     * @param state
     * @param id
     * @returns {boolean}
     */
    setActiveFunction(state, id) {
      const selectedFunctions = state.functions.filter(item => item.selected);
      const isExistNotFilled = selectedFunctions.some(item => item.filled === false);

      // Если передали ID конкретной Ф.О., то её устанавливаем активной.
      if (id) {
        state.activeFunction = selectedFunctions.find(item => item.id === id);
        return false;
      }

      // Если ID не передавали, то устанавливаем активной первую Ф.О. из не оцененных.
      if (isExistNotFilled) {
        state.activeFunction = selectedFunctions.find(item => item.filled === false);
        return false;
      }

      // Если ID не передавали и все Ф.О. оценили, то устанавливаем активной первую Ф.О. из выбранных.
      state.activeFunction = selectedFunctions[0];

    },

    /**
     * Устнавливает подсказку для пользователя.
     * @param state
     * @param payload
     */
    setHint(state, payload) {
      state.currentHints = payload;
    },

    /**
     * Устанавливает query string параметры пользователя из URL.
     * @param state
     * @param payload
     */
    setUserParams(state, payload) {
      state.userParams = {
        cid: Number(payload.cid) || null,
        week: Number(payload.week) || null,
        pid: Number(payload.pid) || null,
        hash: payload.hash || null,
        lang: payload.lang || null,
      };
    },

    /**
     * Устанавливает значения скроллинга для списка оцениваемых Ф.О.
     * @param state
     * @param payload
     */
    setOffsetTopActiveFunction(state, payload) {
      state.offsetTopActiveFunction = payload;
    },

    /**
     * Устанавливает ошибок параметровох оцениваемой Ф.О.
     * @param state
     * @param payload
     */
    setErrorsActiveFunction(state, payload) {
      state.errorsActiveFunction = Object.assign({}, state.errorsActiveFunction, payload);
    },
  },
  actions: {
    /**
     * Валидирует все параметры оцениваемой Ф.о,
     * @param commit
     * @param state
     * @param getters
     * @param successCallback
     * @returns {boolean}
     */
    validationActiveFunction({ commit, state, getters }, successCallback) {
      const func = state.activeFunction;
      const minScore = state.minScore;

      const isFilled = Boolean(func.periodicity && func.processTime && func.helpfulness && func.resultQuality);
      const isMoreMinScore = func.processTime > minScore && func.helpfulness > minScore && func.resultQuality > minScore;
      const isCommentFilled = Boolean(func.comment);

      // Не все поля заполнены
      if (isFilled === false) {
        commit('setErrorsActiveFunction', {
          periodicity: !func.periodicity,
          processTime: !func.processTime,
          helpfulness: !func.helpfulness,
          resultQuality: !func.resultQuality,
          comment: false,
        });

        commit('setHint', state.hints.filter(item => item.id === 5));

        return false;
      }

      // Все поля заполнены && Есть оценки меньше минимальной && Комментарий не заполнен
      if (isFilled === true && isMoreMinScore === false && isCommentFilled === false) {
        commit('setErrorsActiveFunction', {
          periodicity: false,
          processTime: false,
          helpfulness: false,
          resultQuality: false,
          comment: true,
        });

        commit('setHint', state.hints.filter(item => item.id === 6));

        return false;
      }

      // Все поля заполнены && Комментарий заполнен
      // Или все поля заполнены && все оценки больше минимальной
      if (isFilled && isCommentFilled || isFilled && isMoreMinScore) {
        commit('setErrorsActiveFunction', {
          periodicity: false,
          processTime: false,
          helpfulness: false,
          resultQuality: false,
          comment: false,
        });

        successCallback();
      }
    },

    /**
     * Запрашивает данные по Н.Д. и Ф.О.
     * @param context
     */
    getData(context) {
      const userParams = context.state.userParams;
      const querySring = `&cid=${userParams.cid}&week=${userParams.weel}&pid=${userParams.pid}&hash=${userParams.hash}&lang=${userParams.lang}`;

      $.ajax({
        url: `https://in.happy-job.ru/survey/api/functions.php?depth=0${querySring}`,
        method: 'POST',
        async: false,
        crossDomain: false,
        success: (data) => {
          context.commit('setActivities', data);
        },
        error: (xhr, status, errorThrown) => {
          console.log(status);
          console.log(errorThrown);
        },
      });

      $.ajax({
        url: `https://in.happy-job.ru/survey/api/functions.php?depth=1${querySring}`,
        method: 'POST',
        async: false,
        crossDomain: false,
        success: (data) => {
          context.commit('setFunctions', data);
          context.commit('setActiveFunction');
          context.commit('setDataLoaded', true);
        },
        error: (xhr, status, errorThrown) => {
          console.log(status);
          console.log(errorThrown);
        },
      });
    },

    /**
     * Отправляет измененые данные по Н.Д. и Ф.О.
     * @param context
     */
    saveData(context) {
      const data = JSON.stringify({
        interaction: context.getters.getIdsSelectedFunctions,
        params: {
          cid: context.state.userParams.cid,
          week: context.state.userParams.week,
          pid: context.state.userParams.pid,
          hash: context.state.userParams.hash,
          lang: context.state.userParams.lang,
        },
        list: context.getters.getSelectedFunctions,
      });

      $.ajax({
        url: 'https://in.happy-job.ru/survey/api/save.php',
        method: 'POST',
        data,
        contentType: 'application/json',
        async: false,
      });
    },
  },
});
